package com.libquest.p0081viewbyid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        TextView textview1 = findViewById(R.id.textview1);
        textview1.setText("How Wow!");

        Button button1 = findViewById(R.id.button1);
        button1.setText("NewText");
        button1.setEnabled(false);

        CheckBox myChb = findViewById(R.id.myChb);
        myChb.setChecked(true);
    }
}