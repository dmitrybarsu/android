package com.libquest.p0301activity_result;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity  implements View.OnClickListener {

    TextView tvText;
    Button btnColor;
    Button btnAlign;

    final int REQUEST_CODE_COLOR = 1;
    final int REQUEST_CODE_ALIGN = 2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        tvText = (TextView) findViewById(R.id.tvText);
        btnColor = (Button) findViewById(R.id.btnColor);
        btnAlign = (Button) findViewById(R.id.btnAlign);
        btnColor.setOnClickListener(this);
        btnAlign.setOnClickListener(this);
    }

    ActivityResultLauncher<Intent> tvStyleChanger = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    Log.d("myLogs", "resultCode" + result.getResultCode());
                    Intent data = result.getData();
                    assert data != null;

                    switch (result.getResultCode()) {
                        case REQUEST_CODE_COLOR:
                            int color = data.getIntExtra("color", Color.YELLOW);
                            tvText.setTextColor(color);
                            break;
                        case REQUEST_CODE_ALIGN:
                            int align = data.getIntExtra("alignment", Gravity.LEFT);
                            tvText.setGravity(align);
                            break;
                        case RESULT_CANCELED:
                            // что-то пошло не так
                            //Toast.makeText(this, "Wrong result", Toast.LENGTH_LONG).show();
                            break;
                    }
                }
            });

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.btnColor:
                intent = new Intent(this, ColorActivity.class);
                tvStyleChanger.launch(intent);
                break;
            case R.id.btnAlign:
                intent = new Intent(this, AlignActivity.class);
                tvStyleChanger.launch(intent);
                break;
        }
    }
}