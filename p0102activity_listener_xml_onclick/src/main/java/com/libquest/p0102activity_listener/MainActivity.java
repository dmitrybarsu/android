package com.libquest.p0102activity_listener;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements OnClickListener {

    TextView tvOut;
    Button btnOk;
    Button btnCancel;
    Button btnXML;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        tvOut = (TextView) findViewById(R.id.tvOut);
        btnOk = (Button) findViewById(R.id.btnOk);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnXML = (Button) findViewById(R.id.btnXML);

        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnOk:
                tvOut.setText("Нажата кнопка ОК");
                break;
            case R.id.btnCancel:
                tvOut.setText("Нажата кнопка Cancel");
                break;
        }
    }

    public void onClickStart(View v) {
        tvOut.setText("Нажата кнопка XML");
    }
}