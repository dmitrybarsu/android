package com.libquest.p0311intent_data_uri;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnWeb;
    Button btnMap;
    Button btnCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        btnWeb = (Button) findViewById(R.id.btnWeb);
        btnMap = (Button) findViewById(R.id.btnMap);
        btnCall = (Button) findViewById(R.id.btnCall);

        btnWeb.setOnClickListener(this);
        btnMap.setOnClickListener(this);
        btnCall.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.btnWeb:
                // хотим открыть ссылку и ищем для этого подходящее activity
                //           Intent(action, data)
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://developer.android.com"));
                startActivity(intent);
                break;
            case R.id.btnMap:
                // тоже самое, но больше строк кода
                intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("geo:55.754283,37.62002"));
                startActivity(intent);
                break;
            case R.id.btnCall:
                intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:12345"));
                startActivity(intent);
                break;
        }
    }
}

//        Uri uri = Uri.parse("http://developer.android.com/reference/android/net/Uri.html");
//        Log.d("mylogs", uri.getScheme());
//        // http
//        Log.d("mylogs", uri.getSchemeSpecificPart());
//        // //developer.android.com/reference/android/net/Uri.html
//        Log.d("mylogs", uri.getAuthority());
//        // developer.android.com
//        Log.d("mylogs", uri.getHost());
//        //  developer.android.com
//        Log.d("mylogs", uri.getPath());
//        // /reference/android/net/Uri.html
//        Log.d("mylogs", uri.getLastPathSegment());
//        // Uri.html
//
//        Uri uri2 = Uri.parse("tel:12345");
//        // uri.getScheme(): tel
//        // uri.getSchemeSpecificPart():12345
//
//        Uri uri3 = Uri.parse("content://contacts/people/1");
//        // uri.getScheme(): content
//        // uri.getSchemeSpecificPart(): //contacts/people/1
//        // uri.getAuthority(): contacts
//        // uri.getPath(): /people/1
//        // uri.getLastPathSegment(): 1
//
//        // Uri можно создать из абсолютно разных строк:
//        // http-адрес, ftp-адрес, координаты, номер телефона, контакт из адресной книги