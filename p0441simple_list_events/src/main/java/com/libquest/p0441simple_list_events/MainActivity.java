package com.libquest.p0441simple_list_events;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends Activity {

    final String LOG_TAG = "myLogs";

    ListView lvMain;

    /** Called when the activity is first created. */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        lvMain = (ListView) findViewById(R.id.lvMain);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this, R.array.names, android.R.layout.simple_list_item_1);
        lvMain.setAdapter(adapter);

        lvMain.setOnItemClickListener(new OnItemClickListener() {
            // parent=view-родитель, view=нажатый пункт, position=номер пункта,id=id элемента
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(LOG_TAG, "itemClick: position = " + position + ", id = " + id);
            }
        });

        lvMain.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d(LOG_TAG, "itemSelect: position = " + position + ", id = " + id);
            }

            public void onNothingSelected(AdapterView<?> parent) {
                Log.d(LOG_TAG, "itemSelect: nothing");
            }
        });

        lvMain.setOnScrollListener(new AbsListView.OnScrollListener() {
            // обработка состояний прокрутки

            // view = ListView = то, что прокручиваем
            // scrollState – состояние списка. Может принимать три значения:
            // SCROLL_STATE_IDLE = 0, список закончил прокрутку
            // SCROLL_STATE_TOUCH_SCROLL = 1, список начал прокрутку
            // SCROLL_STATE_FLING = 2, список «катнули», т.е.
            // при прокрутке отпустили палец и прокрутка дальше идет «по инерции»
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                 Log.d(LOG_TAG, "scrollState = " + scrollState);
            }

            // обработка прокрутки
            // view – прокручиваемый элемент
            // firstVisibleItem – первый видимый на экране пункт списка
            // visibleItemCount – сколько пунктов видно на экране
            // totalItemCount – сколько всего пунктов в списке
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
//                Log.d(LOG_TAG, "scroll: firstVisibleItem = " + firstVisibleItem
//                        + ", visibleItemCount" + visibleItemCount
//                        + ", totalItemCount" + totalItemCount);
            }
        });

    }
}