package com.libquest.p0291simple_activity_result;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tvName;
    Button btnName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        tvName = findViewById(R.id.tvName);
        btnName = findViewById(R.id.btnName);
        btnName.setOnClickListener(this);
    }

    ActivityResultLauncher<Intent> showUserName = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent data = result.getData();
                        assert data != null;
                        String name = data.getStringExtra("name");
                        tvName.setText(String.format("Your name is %s", name));
                    }
                }
            });

    @Override
    public void onClick(View v) {
        // MainActivity - родитель для NameActivity
        // когда NameActivity закроется в MainActivity будет вызван showUserName
        Intent intent = new Intent(this, NameActivity.class);
        showUserName.launch(intent);
    }
}